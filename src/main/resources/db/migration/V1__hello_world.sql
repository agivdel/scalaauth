CREATE TABLE IF NOT EXISTS public."UserProfiles"
(
     "Id" character varying(256) COLLATE pg_catalog."default" NOT NULL,
      "Roles" json,
      "Age" integer NOT NULL,
      CONSTRAINT "PK_UserProfileTests" PRIMARY KEY ("Id")
);

