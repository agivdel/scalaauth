/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala

import cats.effect._
import cats.implicits._
import com.typesafe.config._
import eu.timepit.refined.auto._
import org.http4s.ember.server._
import org.http4s.server.Router
import org.slf4j.LoggerFactory
import pureconfig._
import sttp.tapir.docs.openapi._
import sttp.tapir.openapi.circe.yaml._
import sttp.tapir.server.http4s._
import sttp.tapir.swagger.http4s.SwaggerHttp4s
import vw.auth_scala.app.config._
import vw.auth_scala.app.controllers.HelloWorldController
import vw.auth_scala.app.db.FlywayDatabaseMigrator

import java.util.concurrent.{ExecutorService, Executors}
import scala.concurrent.ExecutionContext
import pureconfig.generic.semiauto._
import pureconfig.generic.auto._
import vw.auth_scala.app.config.ServiceConfig._
import vw.auth_scala.logic.domain.services.{FilesRepository, TokenParser, UserProfileRepository, UuidRepository}
import vw.auth_scala.logic.primaryAdapters.{AuthService, HelloService}
import vw.auth_scala.logic.secondaryAdapters.{FilesRepositoryCompiler, MyAppCompiler, NimbusTokenParserCompiler, UserProfileRepositoryCompiler, UuidRepositoryCompiler}

object Server extends IOApp.WithContext {
  val blockingPool: ExecutorService = Executors.newFixedThreadPool(2000)
  val ec: ExecutionContext = ExecutionContext.global
  val log = LoggerFactory.getLogger(Server.getClass())
  implicit val clock: java.time.Clock = java.time.Clock.systemUTC()

  override protected def executionContextResource: Resource[SyncIO, ExecutionContext] = Resource.eval(SyncIO(ec))

  override def run(args: List[String]): IO[ExitCode] = {
    val blocker = Blocker.liftExecutorService(blockingPool)
    val migrator = new FlywayDatabaseMigrator

    val config = ConfigFactory.load(getClass().getClassLoader())
    val dbConfig = ConfigSource.fromConfig(config).at(DatabaseConfig.CONFIG_KEY).loadOrThrow[DatabaseConfig]
    val serviceConfig = ConfigSource.fromConfig(config).at(ServiceConfig.CONFIG_KEY).loadOrThrow[ServiceConfig]
    implicit val ac = ConfigSource.fromConfig(config).at(AuthOConfig.CONFIG_KEY).loadOrThrow[AuthOConfig]
    implicit val tokenParserCompiler: NimbusTokenParserCompiler[IO] = new NimbusTokenParserCompiler[IO]()
    implicit val userProfileRepositoryCompiler: UserProfileRepositoryCompiler[IO] = new UserProfileRepositoryCompiler[IO]
    implicit val uuidRepositoryCompiler: UuidRepositoryCompiler[IO] = new UuidRepositoryCompiler[IO]
    implicit val filesRepositoryCompiler = new FilesRepositoryCompiler(Blocker.liftExecutionContext(ec))
    implicit val myAppCompiler: MyAppCompiler = new MyAppCompiler
    implicit val tokenParser: TokenParser = new TokenParser
    implicit val userProfileRepository: UserProfileRepository = new UserProfileRepository()
    implicit val uuidRepository: UuidRepository = new UuidRepository
    implicit val authService: AuthService = new AuthService
    implicit val filesService = new FilesRepository()
    implicit val helloService: HelloService = new HelloService
    val program = for {
      _ <- migrator.migrate(dbConfig.url, dbConfig.user, dbConfig.pass)
      helloWorldEndpoints = new HelloWorldController
      docs = OpenAPIDocsInterpreter().serverEndpointsToOpenAPI(helloWorldEndpoints.endpoints, "auth_scala", "1.0.0")
      swagger = new SwaggerHttp4s(docs.toYaml)
      routes = Http4sServerInterpreter[IO].toRoutes(helloWorldEndpoints.endpoints) <+> swagger.routes[IO]
      httpApp = Router("/" -> routes).orNotFound
      resource = EmberServerBuilder
        .default[IO]
        .withBlocker(blocker)
        .withHost(serviceConfig.ip)
        .withPort(serviceConfig.port)
        .withHttpApp(httpApp)
        .build
      fiber = resource.use(server =>
        IO.delay(log.info("Server started at {}", server.address)) >> IO.never.as(ExitCode.Success)
      )
    } yield fiber
    program.attempt.unsafeRunSync() match {
      case Left(e) =>
        IO {
          log.error("An error occurred during execution!", e)
          ExitCode.Error
        }
      case Right(s) => s
    }
  }

}
