/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.app.config

import eu.timepit.refined.auto._
import pureconfig._
import pureconfig.generic.semiauto._
import pureconfig.generic.auto._
import vw.auth_scala.app.db._
import eu.timepit.refined.auto._
import eu.timepit.refined.predicates.all.Url
import eu.timepit.refined.pureconfig._
import eu.timepit.refined.types.all._
import pureconfig._
import pureconfig.generic.semiauto._
import pureconfig.generic.auto._

final case class AuthOConfig(domain: MyUrl, audience: MyUrl, clientId: NonEmptyString)

object AuthOConfig {
  // The default configuration key to lookup the service configuration.
  final val CONFIG_KEY: ConfigKey = "auth0"

  implicit val serviceConfigReader: ConfigReader[AuthOConfig] = deriveReader[AuthOConfig]
}