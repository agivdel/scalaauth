/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.app.controllers

import cats.effect.IO
import cats.free.Free
import sttp.model.StatusCode
import sttp.tapir._
import vw.auth_scala.app.models.Error
import vw.auth_scala.logic.domain.entities.{AuthToken, User}
import cats.implicits._
import sttp.tapir.codec.refined._
import vw.auth_scala.logic.domain.services._
import vw.auth_scala.logic.primaryAdapters.AuthService
import vw.auth_scala.logic.secondaryAdapters.MyAppCompiler

object BaseController {

  def baseEndpoint = endpoint
    .in("api")
    .in("v1")
    .errorOut(stringBody.and(statusCode).mapTo[Error])

  def baseEndpointWithAuth(implicit authService: AuthService, myAppCompiler: MyAppCompiler) = baseEndpoint
    .in(auth.bearer[AuthToken]())
    .serverLogicForCurrent(authWithToken)

  def authWithToken(token: AuthToken)(implicit authService: AuthService, myAppCompiler: MyAppCompiler): IO[Either[Error, User]] =
    compile(authService.auth(token), StatusCode.Unauthorized)

  def compile[A](free: Free[MyApp, Either[String, A]], code: StatusCode)(implicit myAppCompiler: MyAppCompiler): IO[Either[Error, A]] =
    free.foldMap(myAppCompiler).map(x => x match {
      case Left(msg) => Error(msg, code).asLeft[A]
      case Right(user) => user.asRight[Error]
    })
}
