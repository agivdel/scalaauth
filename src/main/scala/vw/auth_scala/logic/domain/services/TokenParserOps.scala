/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.domain.services

import cats.free.Free
import vw.auth_scala.logic.domain.entities.{AuthToken, UserId}

sealed trait TokenParserOps[A] extends MyApp[A]

case class Parse(token: AuthToken) extends TokenParserOps[Either[String, UserId]]

final class TokenParser {
  def parse(token: AuthToken): Free[MyApp, Either[String, UserId]] = Free.liftF(Parse(token))
}



