/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.domain.services

import cats.free.Free
import doobie.ConnectionIO
import doobie.enumerated.TransactionIsolation
import eu.timepit.refined.types.string.NonEmptyString
import vw.auth_scala.logic.domain.entities.UserProfile

sealed trait UserProfileRepositoryOps[A] extends MyApp[A]

case class Get(id: NonEmptyString) extends UserProfileRepositoryOps[Option[UserProfile]]

case class Add(userProfile: UserProfile) extends UserProfileRepositoryOps[Int]

class UserProfileRepository {
  def get(id: NonEmptyString): Free[MyApp, Option[UserProfile]] =
    Free.liftF(Get(id))

  def add(userProfile: UserProfile): Free[MyApp, Int] =
    Free.liftF(Add(userProfile))
}

sealed trait TransactionFactoryOps[A] extends MyApp[A]

case class Begin(isolation: TransactionIsolation) extends TransactionFactoryOps[Unit]

sealed trait UnitOfWorkOps[A] extends MyApp[A]

sealed class Commit[A](connection: ConnectionIO[A]) extends UnitOfWorkOps[A]

sealed class UnitOfWork {
  def commit[A](connection: ConnectionIO[A]): Free[MyApp, A] =
    Free.liftF(new Commit(connection))
}


