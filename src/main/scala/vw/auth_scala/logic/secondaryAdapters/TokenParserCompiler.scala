/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.secondaryAdapters

import cats.implicits._
import cats.{Applicative, ~>}
import com.auth0.jwk._
import pdi.jwt.{JwtAlgorithm, JwtBase64, JwtCirce, JwtClaim}
import vw.auth_scala.app.config.AuthOConfig
import vw.auth_scala.logic.domain.entities.{AuthToken, UserId}
import vw.auth_scala.logic.domain.services.{Parse, TokenParserOps}

import java.time.Clock
import scala.util.{Failure, Success, Try}



final class TokenParserCompiler[F[_] : Applicative](implicit _config: AuthOConfig, _clock: Clock) extends (TokenParserOps ~> F) {

  def apply[A](fa: TokenParserOps[A]) = fa match {
    case Parse(token) => Applicative[F].pure(parse(token))
  }

  private val jwtRegex = """(.+?)\.(.+?)\.(.+?)""".r

  private def parse(token: AuthToken): Either[String, UserId] = validateJwt(token.toString)
    .toEither match {
    case Right(claim) => claim.subject.fold("Empty sub claim".asLeft[String])(x => x.asRight[String])
      .flatMap(x => UserId.from(x))
    case Left(value) => Left(value.getMessage)
  }


  private def validateJwt(token: String): Try[JwtClaim] = for {
    jwk <- getJwk(token) // Get the secret key for this token
    claims <- JwtCirce.decode(token, jwk.getPublicKey, Seq(JwtAlgorithm.RS256)) // Decode the token using the secret key
    _ <- validateClaims(claims) // validate the data stored inside the token
  } yield claims

  private val splitToken = (jwt: String) => jwt match {
    case jwtRegex(header, body, sig) => Success((header, body, sig))
    case _ => Failure(new Exception("Token does not match the correct pattern"))
  }

  private val decodeElements = (data: Try[(String, String, String)]) => data map {
    case (header, body, sig) =>
      (JwtBase64.decodeString(header), JwtBase64.decodeString(body), sig)
  }

  private val getJwk = (token: String) =>
    (splitToken andThen decodeElements) (token) flatMap {
      case (header, _, _) =>
        val jwtHeader = JwtCirce.parseHeader(header) // extract the header
        val jwkProvider = new UrlJwkProvider(_config.domain.toString)

        // Use jwkProvider to load the JWKS data and return the JWK
        jwtHeader.keyId.map { k =>
          Success(jwkProvider.get(k))
        } getOrElse Failure(new Exception("Unable to retrieve kid"))
    }

  private val validateClaims = (claims: JwtClaim) =>
    if (claims.isValid(_config.domain.toString, _config.audience.toString)) {
      Success(claims)
    } else {
      Failure(new Exception("The JWT did not pass validation"))
    }
}
