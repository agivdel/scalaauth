/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.secondaryAdapters

import cats.effect.{Blocker, Bracket, ContextShift, IO, Sync}
import cats.{Applicative, ~>}
import eu.timepit.refined.auto._
import fs2.io._
import vw.auth_scala.logic.domain.entities._
import vw.auth_scala.logic.domain.services._

import java.nio.file.StandardOpenOption
import doobie._
import doobie.enumerated.TransactionIsolation
import doobie.free.connection.ConnectionOp.Commit
import doobie.implicits._
import doobie.util.transactor.Transactor
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

import java.util.UUID

final class UserProfileRepositoryCompiler[F[_] : Applicative : Sync](implicit xa: Transactor[F], bracket: Bracket[F, Throwable]) extends (UserProfileRepositoryOps ~> F) {
  def apply[A](fa: UserProfileRepositoryOps[A]) =
    fa match {
      case vw.auth_scala.logic.domain.services.Get(id) =>
        bracket.map(get(id.value))(x => x.map(y => UserProfile(UserAge.unsafeFrom(y._3), fromJson[List[String]](y._2))).asInstanceOf[A])
      case Add(userProfile) =>
        bracket.map(add(UUID.randomUUID().toString, userProfile.roles.map(x => x.value).asJson.noSpaces, userProfile.age.value))(_.asInstanceOf[A])
    }

  def get(id: String) = sql"""
        SELECT "Id", "Roles", "Age"
        FROM public."UserProfiles"
         WHERE id = ${id}
         """.query[(String, String, Int)]
    .option.transact(xa)

  def add(id: String, roles: String, age: Int) =
    sql"""
         INSERT INTO public."UserProfiles"("Id", "Roles", "Age")
         	VALUES(${id}, ${roles}, ${age})
         """.update.run.transact(xa)
}

final class TransationFactoryCompiler extends (TransactionFactoryOps ~> ConnectionIO) {
  override def apply[A](fa: TransactionFactoryOps[A]): doobie.ConnectionIO[A] = fa match {
    case Begin(isolation) => HC.setTransactionIsolation(isolation).map(x => x.asInstanceOf[A])
  }
}

final class UnitOfWorkCompiler(implicit xa: Transactor[IO]) extends (UnitOfWorkOps ~> IO) {
  override def apply[A](fa: UnitOfWorkOps[A]): IO[A] = fa match {
    case c: Commit[A] => c.connection.transact(xa)
  }
}




