/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.secondaryAdapters

import cats.{Applicative, ~>}
import vw.auth_scala.logic.domain.services.{Create, UuidRepositoryOps}

import java.util.UUID
final class UuidRepositoryCompiler[F[_] : Applicative] extends (UuidRepositoryOps ~> F) {
  def apply[A](fa: UuidRepositoryOps[A]) =
    fa match {
      case Create() => Applicative[F].pure(UUID.randomUUID())
    }
}


